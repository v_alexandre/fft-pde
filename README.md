# fft-pde

Change Pde to Ode thanks to FFT derivative.
Instead of using finite differences to discretise Bugers's equation we use FFT's properties to get an ODE and integrate our equation.

nonlinear Burger's equation :
```math
u_{t} + uu_{x} = \mu u_{xx}
```

![Burger](Burgertime.png)
![Burger](Burgermap.png)

1D heat equation : 
```math
u_{t} = \alpha u_{xx}
```

![heat](heattime.png)
![heat](heatmap.png)
