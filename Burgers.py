import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import axes3d
import matplotlib.cm as cm

a= 1e-1 #momentum diffusion
L= 10
N= 128
dx= float(L/N)
x= np.arange(-L/2,L/2,dx)

#Define discrete wavenumbers

omega = 2*np.pi*np.fft.fftfreq(N,d=dx)

#Initial condition
u0 = 1/np.cosh(x)

dt=0.001
t = np.arange(0,10,dt)

def rhsBurger(u,t,omega,a):
    uhat = np.fft.fft(u)
    d_uhat = (1j)*omega*uhat
    dd_uhat = -omega**2*uhat
    d_u = np.fft.ifft(d_uhat)
    dd_u = np.fft.ifft(dd_uhat)
    du_dt = -u*d_u + a*dd_u
    return du_dt.real

u = odeint(rhsBurger, u0, t, args=(omega,a))

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlabel('x')
ax.set_ylabel('time')
ax.set_zlabel('Velocity')
plt.set_cmap('jet_r')
Nbre_plots=10
u_plot = u[0:-1:int(len(u)/Nbre_plots),:]

for j in range(u_plot.shape[0]):
    ys = j*np.ones(u_plot.shape[1])
    ax.plot(x,ys,u_plot[j,:],color=cm.jet(j*20))

plt.figure()
plt.imshow(np.flipud(u),aspect=2*N/len(t))
plt.axis('off')
plt.set_cmap('jet_r')
plt.show()
