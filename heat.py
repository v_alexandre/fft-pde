import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import axes3d
import matplotlib.cm as cm

a= 1. #thermal diffu
L= 10
N= 128
dx= float(L/N)
x= np.arange(-L/2,L/2,dx)

#Define discrete wavenumbers

omega = 2*np.pi*np.fft.fftfreq(N,d=dx)

#Initial condition
u0 = np.zeros_like(x)
u0[int((L/2 - L/10)/dx):int((L/2 + L/10)/dx)] = 1
u0hat = np.fft.fft(u0)

u0hat_ri = np.concatenate((u0hat.real,u0hat.imag))

#dt = (L/N)**2*0.1
dt=0.01
t = np.arange(0,10,dt)

def rhsHeat(uhat_ri,t,omega,a):
    uhat = uhat_ri[:N] + (1j)*uhat_ri[N:]
    d_uhat = -a**2 * omega**2 * uhat
    d_uhat_ri = np.concatenate((d_uhat.real, d_uhat.imag)).astype('float')
    return d_uhat_ri

uhat_ri = odeint(rhsHeat, u0hat_ri, t, args=(omega,a))
uhat = uhat_ri[:,:N] + (1j)*uhat_ri[:,N:]

u = np.zeros_like(uhat)

for k in range(len(t)):
    u[k,:] = np.fft.ifft(uhat[k,:])

u = u.real

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlabel('x')
ax.set_ylabel('time')
ax.set_zlabel('Temperature')
plt.set_cmap('jet_r')
Nbre_plots=10
u_plot = u[0:-1:int(len(u)/Nbre_plots),:]

for j in range(u_plot.shape[0]):
    ys = j*np.ones(u_plot.shape[1])
    ax.plot(x,ys,u_plot[j,:],color=cm.jet(j*20))

plt.figure()
plt.imshow(np.flipud(u),aspect=2*N/len(t))
plt.axis('off')
plt.set_cmap('jet_r')
plt.show()
